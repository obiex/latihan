import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';

export default class App extends Component {
  render() {
    return (
      <View style={styles.viewContainer}>
        <View style={styles.viewWrapper}>
          <Image
            source={{
              uri:
                'https://codelatte.org/wp-content/uploads/2018/07/fixcil.png',
            }}
            style={styles.imageLogin}
          />
          <Text style={styles.textTitle}>LOGIN HERE</Text>
        </View>
        <TextInput placeholder="Masukan Email" style={styles.textInput} />
        <TextInput placeholder="No tlp" style={styles.textInput} />
        <TextInput
          placeholder="Masukan Password"
          style={styles.textInput}
          secureTextEntry
        />
        <TouchableOpacity>
          <View style={styles.viewButton}>
            <Text style={styles.textLogin}>LOGIN</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  textInput: {
    width: responsiveWidth(90),
    height: responsiveHeight(7),
    borderWidth: 1,
    borderColor: '#DDD',
    borderRadius: 10,
    alignSelf: 'center',
    marginVertical: responsiveHeight(2.33),
  },
  viewButton: {
    width: responsiveWidth(90),
    height: responsiveHeight(7),
    alignItems: 'center',
    backgroundColor: '#43B2EC',
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 10,
    marginTop: responsiveHeight(1.1),
  },
  textLogin: {
    fontWeight: 'bold',
    color: '#FFF',
    fontSize: responsiveFontSize(2.2),
  },
  imageLogin: {
    width: 100,
    height: 100,
  },
  viewWrapper: {
    alignItems: 'center',
  },
  textTitle: {
    fontSize: responsiveFontSize(2.2),
    fontWeight: 'bold',
    color: '#43B2EC',
  },
});